import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

/**
 * Generated class for the MenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {

  nombre: string = "";
  searchQuery: string = "";
  items: string[];

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController) {
    this.nombre = navParams.get("name");
  }

  ionViewDidLoad() {
    this.initializeItems();
  }
  initializeItems() {
    this.items = [
      'Jujuy',
      'Salta',
      'Chubut',
      'Cordoba'
    ]
  }

  getItems(ev: any) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the searchbar
    const val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.items = this.items.filter((item) => {
        return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  showalert(){
    let alert = this.alertCtrl.create({
      title: 'new Friend',
      subTitle: 'Your friend, just accept your friend',
      buttons: ['OK']
    });

    alert.present();
  }

}
