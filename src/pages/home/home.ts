import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Pagina2Page } from '../pagina2/pagina2';
import { MenuPage } from '../menu/menu';
import { AlertController} from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  nombre: string = "";

  constructor(public navCtrl: NavController, public alertctrl: AlertController) {

  }

  goToPagina2():void{
    this.navCtrl.push(Pagina2Page);
  }

  goToMenu():void{
    if(this.nombre == ''){
      let alert = this.alertctrl.create({
        title: 'Noificación ',
        subTitle: 'El campo no debe estar vacio',
        buttons: ['OK']
      });
      alert.present();
    }else{
      this.navCtrl.push(MenuPage, {name: this.nombre});
    }
  }
}
