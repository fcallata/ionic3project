import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpClient} from '@angular/common/http'
import { Observable } from 'rxjs/Observable';
import {AlertController} from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-pagina2',
  templateUrl: 'pagina2.html',
})
export class Pagina2Page {

  data: Observable<any>;
  items: any;
  url: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public httpClient: HttpClient, public alertCtrl : AlertController) {
    this.url = "";    
  }

  ionViewDidLoad() {
    this.loadUser();
  }

  volver():void{
    this.navCtrl.pop();
  }

  loadUser(){
    console.log('Hola method');
    this.data = this.httpClient.get('https://jsonplaceholder.typicode.com/users');
    this.data
    .subscribe(data => {
      console.log('my data: ', data);
      this.items = data;
    })
  }

  alertList(id){

    let vlist = this.items.filter(function(e,i){ return e.id == id})[0];

    let alert = this.alertCtrl.create({
      title: 'Detalle: ' + vlist.id,
      subTitle: 'This a subtitle ' + vlist.name,
      buttons: ['OK']
    });
    alert.present();
  }

}
